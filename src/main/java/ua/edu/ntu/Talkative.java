package ua.edu.ntu;

public interface Talkative {
    String say();
}
