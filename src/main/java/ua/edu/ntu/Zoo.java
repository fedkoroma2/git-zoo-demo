package ua.edu.ntu;

import java.util.ArrayList;
import java.util.List;

public class Zoo {
    private final List<Animal> animals;

    public Zoo() {
        animals = new ArrayList<>();
        animals.add(new Fox());
    }

    public void printAllAnimals() {
        System.out.println("We have " + animals.size() + " animal in the zoo.");
        for (Animal animal : animals) {
            System.out.println(animal.getClass().getSimpleName() + " says " + animal.say());
        }
    }
}
